import React, { Component } from 'react';
import './App.css';
import { Theme } from 'zd-components';
import { map, compose, prop } from 'ramda';
import _ from 'lodash';
import ZDheader from './components/ZDheader';
import TableView from './components/TableView';
import CardView from './components/CardView';
import InnerHeader from './components/InnerHeader';
import SidePanel from './components/SidePanel';
import './styles.css';

/*
NOTES:

Building this was relatively quick for a couple of reasons.
	1: Using a HTML to JSX tool online, I was able to quickly compile JSX code from
	   Qi's version of the home page.
	2: Using CSS modules (like './styles' on line 11) I am able to use Qi's CSS

Other:
	You can toggle between Grid View and Table View by clicking the upper right button toggle
	You can set the number of 'Cards' in Grid View via the cardCount prop
	Grid.css and Column.jsx can be cut - there are some styles there though that would need to be extracted.

 */


class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			defaultView: true
		};
		_.bindAll(this, ['handleView'])
	}
	handleView(e) {
		const { defaultView } = this.state;
		this.setState({defaultView: !defaultView})
	}
  render() {
	  const ListItems = ['View All', 'Favorites', 'My Items', 'Shared With Me'];

	  return (
      <Theme>
      <div>
	      <ZDheader />
	      <div id="main">
		      {SidePanel(ListItems)}
		      <div id="content">
			      <div className="content_top" style={{zIndex: 200, display: 'block'}}>
				      <InnerHeader onClick={this.handleView}/>
			      </div>
			      <div className="content_body" style={{zIndex: 100}}>
				      {this.state.defaultView ? <CardView cardCount={45} /> : <TableView/>}
			      </div>
		      </div>
	      </div>
      </div>
      </Theme>
    );
  }
}

export default App;

/*


 const mainPanelStyles = {
 position: 'absolute',
 top: '0px',
 right: '0px',
 width: 'calc(100% - 145px)',
 height: '100%',
 backgroundColor: '#e6e6e6',
 zIndex: 100,
 };



 const MainPanel = ({}) =>
 <Column width="ten" style={mainPanelStyles}>
 <div>
 Some Text Here
 </div>
 </Column>;
 */