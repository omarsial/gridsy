import React, {
	Component,
	PropTypes,
} from 'react';

import '../styles.css';


const Card = (idx) =>
  <li className="itemSet favoritesSection sharedFilesSection customerTagSection"
      style={{width: '296.5px', height: '198.781px', margin: '12.9208px 5px'}}
      key={idx}
  >
	  <div className="itemSet_main"
	       style={{width: '296.5px', height: '166.781px', boxShadow: 'rgba(0, 0, 0, 0.2) 2px 2px 2px 0px'}}>
		  <div className="itemSet_main_img">
			  <img src="https://www.alr-research.com/styleguide_beta/img/Home/FloatingBubbles_1.jpg" alt="Dashboard"
			       width="100%" height="100%"/>
		  </div>
		  <div className="itemSet_main_info" style={{opacity: 0}}>
			  <div className="inside_contet_container">
				  <div className="inside_item_name">Resources Comparison</div>
				  <div className="inside_modifiedDate_calc">10 mins ago</div>
				  <div className="dataSource_and_creator_container">
					  <div className="inside_dataSource">
						  <span className="dafonticon"></span>My Cloudera Data
					  </div>
					  <div className="inside_creator"><span className="dafonticon"></span>Matthew Weber</div>
				  </div>
				  <div className="inside_description"><span className="dafonticon"></span>Lorem ipsum dolor sit amet,
					  consectetur adipiscing elit.
				  </div>
			  </div>
			  <div className="inside_tools_container">
				  <div className="inside_star"><span></span></div>
				  <div className="inside_delete"><span><span /></span></div>
			  </div>
		  </div>
	  </div>
	  <div className="itemSet_title" style={{opacity: 1}}>
		  <div className="outside_item_name">Resources Comparison</div>
		  <div className="outside_modifiedDate_calc">10 mins ago</div>
	  </div>
  </li>;

class CardView extends Component {
	render() {
		const { cardCount } = this.props;
		return (
		  <div className="thumbnailWrapper">
			  <div className="rowSet">
				  <div className="row clearfix" style={{paddingTop: '27.0792px'}}>
					  {Array(cardCount).fill('a').map((f, i) => Card(i))}
				  </div>
			  </div>
		  </div>
		);
	}
}

CardView.propTypes = {
	cardCount: PropTypes.number
};
CardView.defaultProps = {
	cardCount: 5,
};

export default CardView;
