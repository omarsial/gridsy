import React, { Component, PropTypes } from 'react';
import { compose, map, prop } from 'ramda'
import './Grid.css'

const styles = {
	col: {
		float: 'left',
		paddingRight: '20px'
	}
};

const columnWidths  = {
	one: `${100/12}%`,
	two: `${200/12}%`,
	three: `${300/12}%`,
	four: `${400/12}%`,
	five: `${500/12}%`,
	six: `${600/12}%`,
	seven: `${700/12}%`,
	eight: `${800/12}%`,
	nine: `${900/12}%`,
	ten: `${1000/12}%`,
	eleven: `${1100/12}%`,
	twelve: `${1200/12}%`,
};

const getColWidth = (w, st) => {
	const { float, paddingRight } = styles.col;
	const width = columnWidths[w];
	// debugger;
	return { float, width, ...st };
};

const Column = ({children, width, style, gutter}) =>
  <div style={getColWidth(width, style)} className="col">
	  {children}
  </div>;

// const FinalList = compose(Grid, map(), prop('listItems'));

Column.propTypes = {};

export default Column;
