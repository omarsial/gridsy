import React, {
	Component,
	PropTypes,
} from 'react';

import '../styles.css';

class InnerHeader extends Component {
	render() {
		return (

		  <div className="content_top_wrapper">
			  <div className="iconBtn iconBtn_medium iconBtn_readyOnly">
				  
			  </div>
			  <div className="layoutSet_title label_9">
				  View All
			  </div>
			  <div className="btnToggleSet">
				  <div className="thumbnailViewBtn btnToggle btnToggle_icon btnToggle_icon_thumbNail btnToggle_selected">
					  
				  </div>
				  <div className="listViewBtn btnToggle btnToggle_icon btnToggle_icon_list"
				    onClick={this.props.onClick}
				  >
					  
				  </div>
			  </div>
			  <div className="sortToggleSet">
				  <div className="sortToggleSet_label">
					  <div className="label_3">
						  Sort By
					  </div>
				  </div>
				  <div className="sortToggleSet_dropDown">
					  <div className="dropDownLink linkSet linkSet_primary">
						  <div className="linkSet_label">Last Modified</div>
					  </div>
				  </div>
			  </div>
		  </div>

		);
	}
}

InnerHeader.propTypes = {
	onClick: PropTypes.func
};
InnerHeader.defaultProps = {
};

export default InnerHeader;
