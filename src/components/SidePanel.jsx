import React, { Component } from 'react';
import '../styles.css';
import { Theme, Button, FontIcon, SortToggle } from 'zd-components';
import { map, compose, prop } from 'ramda';
import Column from './Column'


const sidePanelStyles = {
	'boxSizing': 'borderBox',
	'position': 'absolute',
	'top': '0px',
	'left': '0px',
	// 'paddingTop': '10px',
	// 'paddingBottom': '10px',
	'width': '145px',
	'height': '100%',
	'backgroundColor': '#595959',
	'zIndex': '100',
};

const SidePanelListItem = label =>
  <div className="sideNavBtn" id="favoritesBtn">
	  <div className="linkSet linkSet_secondary">
		  <div className="linkSet_icon"></div>
		  <div className="linkSet_label">{label}</div>
	  </div>
  </div>;

const SidePanel = items =>
  <Column style={sidePanelStyles}>
	  <div id="sideNavMenu">
		  <div id="sideNavMenu_top">
			  <div id="addNewBtn" className="btn_mediun btn_primary">
				  <div className="btn_mediun_icon"></div>
				  <div className="btn_mediun_label">New</div>
			  </div>
		  </div>
		  <div id="sideNavMenu_search">
			  <div id="sideNavMenu_searchBtn" className="linkSet linkSet_secondary">
				  <div className="linkSet_icon"></div>
				  <div className="linkSet_label">Search</div>
			  </div>
			  <div id="sideNavMenu_searchInput" className="inputBox">
				  <input type="text" placeholder="Search" />
			  </div>
			  <div id="sideNavMenu_searchClose" className="iconBtn iconBtn_small iconBtn_secondary">
				  
			  </div>
		  </div>
		  <div id="sideNavMenu_body">
			  {items}
		  </div>
	  </div>
  </Column>;

// trying functional composition here: should we do more of this? thoughts...
const ComposeSidePanel = compose(SidePanel, map(SidePanelListItem));

export default ComposeSidePanel;

