import React, {
	Component,
	PropTypes,
} from 'react';

import '../styles.css';

// I only pasted the html code in here after parsing it into JSX, sorry for the formatting

class TableView extends Component {
	render() {
		return (
		  <div className="listWrapper" style={{display: 'block'}}>
			  <div className="listWrapper_leftSection">
				  <div className="listWrapper_tableHeaderFixed">
					  <table className="home_table">
						  <thead>
						  <tr>
							  <th className="column_05" style={{width: 18}}>
								  <div className="label_2">Fav</div>
							  </th>
							  <th className="column_30" colSpan={2}>
								  <div className="label_2">Name</div>
							  </th>
							  <th className="column_25" colSpan={2}>
								  <div className="label_2">Data&nbsp;Source</div>
							  </th>
							  <th className="column_20">
								  <div className="label_2">Owner</div>
							  </th>
							  <th className="column_20" style={{minWidth: 114}}>
								  <div className="label_2">Last&nbsp;Modified</div>
								  <div className="table_headerSort">
									  <div className="sortToggle">
										  <div className="sortArrow sortArrowUp">
										  </div>
										  &lt;<div className="sortArrow sortArrowDown_Selected">
									  </div>
									  </div>
								  </div>
							  </th>
						  </tr>
						  </thead>
						  <tbody>
						  <tr className="tr_notSelected">
							  <td style={{width: 18}}>
								  <div className="iconBtn iconBtn_small iconBtn_secondary"></div>
							  </td>
							  <td style={{width: 14}}>
								  <div className="iconBtn iconBtn_small iconBtn_primary"></div>
							  </td>
							  <td style={{paddingLeft: 0}}>
								  <div className="table_component linkSet linkSet_primary">
									  <div className="linkSet_label label_overflow_1lines">
										  Dashboard for Sales Team
									  </div>
								  </div>
							  </td>
							  <td>
								  <div className="table_component label_3 label_overflow_1lines">
									  My Cloudera Data
								  </div>
							  </td>
							  <td className="td_textAlignCenter" style={{width: 24}}>
								  <div className="iconBtn iconBtn_small iconBtn_secondary"></div>
							  </td>
							  <td>
								  <div className="table_component label_3 label_overflow_1lines">
									  Matthew Weber
								  </div>
							  </td>
							  <td>
								  <div className="table_component label_3 label_overflow_1lines">
									  Aug 15, 2016, 11:10 AM
								  </div>
							  </td>
						  </tr>
						  </tbody>
					  </table>
				  </div>
				  <div className="listWrapper_table">
					  <table className="home_table">
						  <thead>
						  <tr>
							  <th className="column_05" style={{width: 18}}>
								  <div className="label_2 ">Fav</div>
							  </th>
							  <th className="column_30" colSpan={2}>
								  <div className="label_2 ">Name</div>
							  </th>
							  <th className="column_25" colSpan={2}>
								  <div className="label_2 ">Data&nbsp;Source</div>
							  </th>
							  <th className="column_20">
								  <div className="label_2 ">Owner</div>
							  </th>
							  <th className="column_20" style={{minWidth: 114}}>
								  <div className="label_2 ">Last&nbsp;Modified</div>
								  <div className="table_headerSort">
									  <div className="sortToggle">
										  <div className="sortArrow sortArrowUp">
										  </div>
										  &lt;<div className="sortArrow sortArrowDown_Selected">
									  </div>
									  </div>
								  </div>
							  </th>
						  </tr>
						  </thead>
						  <tbody>
						  <tr id="item_1" className="fileRow tr_notSelected sharedFilesSection customerTagSection favoritesSection"><td><div className="iconBtn iconBtn_small iconBtn_secondary"></div></td><td className="td_textAlignCenter"><div className="iconBtn iconBtn_small iconBtn_primary"></div></td><td style={{paddingLeft: 0}}><div className="table_component linkSet linkSet_primary"><div className="linkSet_label label_overflow_1lines">Resources Comparison</div></div></td><td><div className="table_component label_3 label_overflow_1lines">My Cloudera Data</div></td><td className="td_textAlignCenter"><div className="iconBtn iconBtn_small iconBtn_secondary" /></td><td><div className="table_component label_3 label_overflow_1lines">Matthew Weber</div></td><td><div className="table_component label_3 label_overflow_1lines">Aug 22, 2016, 4:00 PM</div></td></tr><tr id="item_2" className="fileRow tr_notSelected sharedFilesSection"><td><div className="iconBtn iconBtn_small iconBtn_secondary"></div></td><td className="td_textAlignCenter"><div className="iconBtn iconBtn_small iconBtn_primary"></div></td><td style={{paddingLeft: 0}}><div className="table_component linkSet linkSet_primary"><div className="linkSet_label label_overflow_1lines">Sales Overview</div></div></td><td><div className="table_component label_3 label_overflow_1lines">Real Time Sales</div></td><td className="td_textAlignCenter"><div className="iconBtn iconBtn_small iconBtn_secondary" /></td><td><div className="table_component label_3 label_overflow_1lines">Burton Yount</div></td><td><div className="table_component label_3 label_overflow_1lines">Aug 22, 2016, 3:00 PM</div></td></tr><tr id="item_3" className="fileRow tr_notSelected myFilesSection"><td><div className="iconBtn iconBtn_small iconBtn_secondary"></div></td><td className="td_textAlignCenter"><div className="iconBtn iconBtn_small iconBtn_primary"></div></td><td style={{paddingLeft: 0}}><div className="table_component linkSet linkSet_primary"><div className="linkSet_label label_overflow_1lines">Fall Campaign</div></div></td><td><div className="table_component label_3 label_overflow_1lines">My Cloudera Data</div></td><td className="td_textAlignCenter"><div className="iconBtn iconBtn_small iconBtn_secondary" /></td><td><div className="table_component label_3 label_overflow_1lines">Qi Zheng</div></td><td><div className="table_component label_3 label_overflow_1lines">Aug 22, 2016, 2:10 PM</div></td></tr><tr id="item_4" className="fileRow tr_notSelected myFilesSection modelsTagSection"><td><div className="iconBtn iconBtn_small iconBtn_secondary"></div></td><td className="td_textAlignCenter"><div className="iconBtn iconBtn_small iconBtn_primary"></div></td><td style={{paddingLeft: 0}}><div className="table_component linkSet linkSet_primary"><div className="linkSet_label label_overflow_1lines">Sales Demo</div></div></td><td><div className="table_component label_3 label_overflow_1lines">Hotel Data</div></td><td className="td_textAlignCenter"><div className="iconBtn iconBtn_small iconBtn_secondary" /></td><td><div className="table_component label_3 label_overflow_1lines">Qi Zheng</div></td><td><div className="table_component label_3 label_overflow_1lines">Aug 22, 2016, 1:00 PM</div></td></tr><tr id="item_5" className="fileRow tr_notSelected myFilesSection 2016TagSection newTagSection salesTagSection"><td><div className="iconBtn iconBtn_small iconBtn_secondary"></div></td><td className="td_textAlignCenter"><div className="iconBtn iconBtn_small iconBtn_primary"></div></td><td style={{paddingLeft: 0}}><div className="table_component linkSet linkSet_primary"><div className="linkSet_label label_overflow_1lines">Campaign 2016</div></div></td><td><div className="table_component label_3 label_overflow_1lines">Hotel Data</div></td><td className="td_textAlignCenter"><div className="iconBtn iconBtn_small iconBtn_secondary" /></td><td><div className="table_component label_3 label_overflow_1lines">Qi Zheng</div></td><td><div className="table_component label_3 label_overflow_1lines">Aug 22, 2016, 9:30 AM</div></td></tr><tr id="item_6" className="fileRow tr_notSelected sharedFilesSection"><td><div className="iconBtn iconBtn_small iconBtn_secondary"></div></td><td className="td_textAlignCenter"><div className="iconBtn iconBtn_small iconBtn_primary"></div></td><td style={{paddingLeft: 0}}><div className="table_component linkSet linkSet_primary"><div className="linkSet_label label_overflow_1lines">Sales by States</div></div></td><td><div className="table_component label_3 label_overflow_1lines">Hotel Data</div></td><td className="td_textAlignCenter"><div className="iconBtn iconBtn_small iconBtn_secondary" /></td><td><div className="table_component label_3 label_overflow_1lines">Burton Yount</div></td><td><div className="table_component label_3 label_overflow_1lines">Aug 21, 2016, 3:30 PM</div></td></tr><tr id="item_7" className="fileRow tr_notSelected myFilesSection databaseTagSection marketingTagSection"><td><div className="iconBtn iconBtn_small iconBtn_secondary"></div></td><td className="td_textAlignCenter"><div className="iconBtn iconBtn_small iconBtn_primary"></div></td><td style={{paddingLeft: 0}}><div className="table_component linkSet linkSet_primary"><div className="linkSet_label label_overflow_1lines">Market Analysis</div></div></td><td><div className="table_component label_3 label_overflow_1lines">Real Time Sales</div></td><td className="td_textAlignCenter"><div className="iconBtn iconBtn_small iconBtn_secondary" /></td><td><div className="table_component label_3 label_overflow_1lines">Qi Zheng</div></td><td><div className="table_component label_3 label_overflow_1lines">Aug 21, 2016, 1:30 PM</div></td></tr><tr id="item_8" className="fileRow tr_notSelected myFilesSection partnerTagSection"><td><div className="iconBtn iconBtn_small iconBtn_secondary"></div></td><td className="td_textAlignCenter"><div className="iconBtn iconBtn_small iconBtn_primary"></div></td><td style={{paddingLeft: 0}}><div className="table_component linkSet linkSet_primary"><div className="linkSet_label label_overflow_1lines">Ticket Event</div></div></td><td><div className="table_component label_3 label_overflow_1lines">Ticket Fusion</div></td><td className="td_textAlignCenter"><div className="iconBtn iconBtn_small iconBtn_secondary" /></td><td><div className="table_component label_3 label_overflow_1lines">Qi Zheng</div></td><td><div className="table_component label_3 label_overflow_1lines">Aug 20, 2016, 9:00 AM</div></td></tr><tr id="item_9" className="fileRow tr_notSelected sharedFilesSection infoTagSection"><td><div className="iconBtn iconBtn_small iconBtn_secondary"></div></td><td className="td_textAlignCenter"><div className="iconBtn iconBtn_small iconBtn_primary"></div></td><td style={{paddingLeft: 0}}><div className="table_component linkSet linkSet_primary"><div className="linkSet_label label_overflow_1lines">Dashboard for Sales Team</div></div></td><td><div className="table_component label_3 label_overflow_1lines">3 Data Sources</div></td><td className="td_textAlignCenter"><div className="dataSourceInfoIcon iconBtn iconBtn_small iconBtn_secondary"></div></td><td><div className="table_component label_3 label_overflow_1lines">Matthew Weber</div></td><td><div className="table_component label_3 label_overflow_1lines">Aug 19, 2016, 3:00 PM</div></td></tr><tr id="item_10" className="fileRow tr_notSelected myFilesSection favoritesSection"><td><div className="iconBtn iconBtn_small iconBtn_secondary"></div></td><td className="td_textAlignCenter"><div className="iconBtn iconBtn_small iconBtn_primary"></div></td><td style={{paddingLeft: 0}}><div className="table_component linkSet linkSet_primary"><div className="linkSet_label label_overflow_1lines">Team Summary</div></div></td><td><div className="table_component label_3 label_overflow_1lines">Hotel Data</div></td><td className="td_textAlignCenter"><div className="iconBtn iconBtn_small iconBtn_secondary" /></td><td><div className="table_component label_3 label_overflow_1lines">Qi Zheng</div></td><td><div className="table_component label_3 label_overflow_1lines">Aug 19, 2016, 2:00 PM</div></td></tr><tr id="item_11" className="fileRow tr_notSelected myFilesSection databaseTagSection favoritesSection"><td><div className="iconBtn iconBtn_small iconBtn_secondary"></div></td><td className="td_textAlignCenter"><div className="iconBtn iconBtn_small iconBtn_primary"></div></td><td style={{paddingLeft: 0}}><div className="table_component linkSet linkSet_primary"><div className="linkSet_label label_overflow_1lines">Details Analysis</div></div></td><td><div className="table_component label_3 label_overflow_1lines">My Cloudera Data</div></td><td className="td_textAlignCenter"><div className="iconBtn iconBtn_small iconBtn_secondary" /></td><td><div className="table_component label_3 label_overflow_1lines">Qi Zheng</div></td><td><div className="table_component label_3 label_overflow_1lines">Aug 18, 2016, 12:00 AM</div></td></tr><tr id="item_12" className="fileRow tr_notSelected myFilesSection companyTagSection modelsTagSection favoritesSection"><td><div className="iconBtn iconBtn_small iconBtn_secondary"></div></td><td className="td_textAlignCenter"><div className="iconBtn iconBtn_small iconBtn_primary"></div></td><td style={{paddingLeft: 0}}><div className="table_component linkSet linkSet_primary"><div className="linkSet_label label_overflow_1lines">Real Time Sales 1</div></div></td><td><div className="table_component label_3 label_overflow_1lines">Real Time Sales</div></td><td className="td_textAlignCenter"><div className="iconBtn iconBtn_small iconBtn_secondary" /></td><td><div className="table_component label_3 label_overflow_1lines">Qi Zheng</div></td><td><div className="table_component label_3 label_overflow_1lines">Aug 17, 2016, 11:10 AM</div></td></tr><tr id="item_13" className="fileRow tr_notSelected sharedFilesSection customerTagSection"><td><div className="iconBtn iconBtn_small iconBtn_secondary"></div></td><td className="td_textAlignCenter"><div className="iconBtn iconBtn_small iconBtn_primary"></div></td><td style={{paddingLeft: 0}}><div className="table_component linkSet linkSet_primary"><div className="linkSet_label label_overflow_1lines">Customer Overview</div></div></td><td><div className="table_component label_3 label_overflow_1lines">Real Time Sales</div></td><td className="td_textAlignCenter"><div className="iconBtn iconBtn_small iconBtn_secondary" /></td><td><div className="table_component label_3 label_overflow_1lines">Matthew Weber</div></td><td><div className="table_component label_3 label_overflow_1lines">Aug 17, 2016, 10:10 AM</div></td></tr><tr id="item_14" className="fileRow tr_notSelected sharedFilesSection customerTagSection marketingTagSection"><td><div className="iconBtn iconBtn_small iconBtn_secondary"></div></td><td className="td_textAlignCenter"><div className="iconBtn iconBtn_small iconBtn_primary"></div></td><td style={{paddingLeft: 0}}><div className="table_component linkSet linkSet_primary"><div className="linkSet_label label_overflow_1lines">Ticket Sales Analysis</div></div></td><td><div className="table_component label_3 label_overflow_1lines">Ticket Fusion</div></td><td className="td_textAlignCenter"><div className="iconBtn iconBtn_small iconBtn_secondary" /></td><td><div className="table_component label_3 label_overflow_1lines">Matthew Weber</div></td><td><div className="table_component label_3 label_overflow_1lines">Aug 17, 2016, 9:00 AM</div></td></tr><tr id="item_15" className="fileRow tr_notSelected sharedFilesSection databaseTagSection"><td><div className="iconBtn iconBtn_small iconBtn_secondary"></div></td><td className="td_textAlignCenter"><div className="iconBtn iconBtn_small iconBtn_primary"></div></td><td style={{paddingLeft: 0}}><div className="table_component linkSet linkSet_primary"><div className="linkSet_label label_overflow_1lines">Real Time Sales 2</div></div></td><td><div className="table_component label_3 label_overflow_1lines">Real Time Sales</div></td><td className="td_textAlignCenter"><div className="iconBtn iconBtn_small iconBtn_secondary" /></td><td><div className="table_component label_3 label_overflow_1lines">Matthew Weber</div></td><td><div className="table_component label_3 label_overflow_1lines">Aug 17, 2016, 8:30 AM</div></td></tr><tr id="item_16" className="fileRow tr_notSelected sharedFilesSection companyTagSection partnerTagSection"><td><div className="iconBtn iconBtn_small iconBtn_secondary"></div></td><td className="td_textAlignCenter"><div className="iconBtn iconBtn_small iconBtn_primary"></div></td><td style={{paddingLeft: 0}}><div className="table_component linkSet linkSet_primary"><div className="linkSet_label label_overflow_1lines">Real Time Sales 3</div></div></td><td><div className="table_component label_3 label_overflow_1lines">Real Time Sales</div></td><td className="td_textAlignCenter"><div className="iconBtn iconBtn_small iconBtn_secondary" /></td><td><div className="table_component label_3 label_overflow_1lines">Burton Yount</div></td><td><div className="table_component label_3 label_overflow_1lines">Aug 16, 2016, 3:30 PM</div></td></tr><tr id="item_17" className="fileRow tr_notSelected sharedFilesSection companyTagSection conferenceTagSection favoritesSection"><td><div className="iconBtn iconBtn_small iconBtn_secondary"></div></td><td className="td_textAlignCenter"><div className="iconBtn iconBtn_small iconBtn_primary"></div></td><td style={{paddingLeft: 0}}><div className="table_component linkSet linkSet_primary"><div className="linkSet_label label_overflow_1lines">Ticket Dashboard</div></div></td><td><div className="table_component label_3 label_overflow_1lines">Ticket Fusion</div></td><td className="td_textAlignCenter"><div className="iconBtn iconBtn_small iconBtn_secondary" /></td><td><div className="table_component label_3 label_overflow_1lines">Burton Yount</div></td><td><div className="table_component label_3 label_overflow_1lines">Aug 16, 2016, 11:10 AM</div></td></tr><tr id="item_18" className="fileRow tr_notSelected myFilesSection infoTagSection"><td><div className="iconBtn iconBtn_small iconBtn_secondary"></div></td><td className="td_textAlignCenter"><div className="iconBtn iconBtn_small iconBtn_primary"></div></td><td style={{paddingLeft: 0}}><div className="table_component linkSet linkSet_primary"><div className="linkSet_label label_overflow_1lines">Securities Analysis</div></div></td><td><div className="table_component label_3 label_overflow_1lines">2 Data Sources</div></td><td className="td_textAlignCenter"><div className="dataSourceInfoIcon iconBtn iconBtn_small iconBtn_secondary"></div></td><td><div className="table_component label_3 label_overflow_1lines">Qi Zheng</div></td><td><div className="table_component label_3 label_overflow_1lines">Aug 16, 2016, 10:00 AM</div></td></tr><tr id="item_19" className="fileRow tr_notSelected sharedFilesSection conferenceTagSection customerTagSection favoritesSection"><td><div className="iconBtn iconBtn_small iconBtn_secondary"></div></td><td className="td_textAlignCenter"><div className="iconBtn iconBtn_small iconBtn_primary"></div></td><td style={{paddingLeft: 0}}><div className="table_component linkSet linkSet_primary"><div className="linkSet_label label_overflow_1lines">Performance Overview</div></div></td><td><div className="table_component label_3 label_overflow_1lines">Hotel Data</div></td><td className="td_textAlignCenter"><div className="iconBtn iconBtn_small iconBtn_secondary" /></td><td><div className="table_component label_3 label_overflow_1lines">Matthew Weber</div></td><td><div className="table_component label_3 label_overflow_1lines">Aug 15, 2016, 9:30 AM</div></td></tr><tr id="item_20" className="fileRow tr_notSelected myFilesSection companyTagSection marketingTagSection"><td><div className="iconBtn iconBtn_small iconBtn_secondary"></div></td><td className="td_textAlignCenter"><div className="iconBtn iconBtn_small iconBtn_primary"></div></td><td style={{paddingLeft: 0}}><div className="table_component linkSet linkSet_primary"><div className="linkSet_label label_overflow_1lines">Store Locations</div></div></td><td><div className="table_component label_3 label_overflow_1lines">Real Time Sales</div></td><td className="td_textAlignCenter"><div className="iconBtn iconBtn_small iconBtn_secondary" /></td><td><div className="table_component label_3 label_overflow_1lines">Qi Zheng</div></td><td><div className="table_component label_3 label_overflow_1lines">Aug 15, 2016, 9:20 AM</div></td></tr></tbody>
					  </table>
				  </div>
			  </div>
			  <div className="listWrapper_rightSection">
				  <div className="listWrapper_preview_header">
					  <div className="label_2 ">Preview</div>
				  </div>
				  <div className="listWrapper_preview_body">
					  <div className="listWrapper_preview_empty">
						  <div className="label_3">Please select an item to preview.</div>
					  </div>
					  <div className="listWrapper_preview_content">
						  <div id="item_thumbnail" className="listWrapper_preview_thumbnail">
							  <img src="../img/Home/Bar_2.jpg" alt="Bars" height="100%" width="100%" />
							  <div className="listWrapper_preview_thumbnail_mask" />
						  </div>
						  <div className="listWrapper_preview_info">
							  <div id="item_name" className="item_info">
								  <div className="layoutSet_horizontal">
									  <div className="layoutSet_horizontal_group clearfix">
										  <div className="layoutSet_horizontal_label label_2">
											  Name:
										  </div>
										  {/*                       <div class="table_component linkSet linkSet_primary">
										   <div class="linkSet_label label_overflow_1lines">
										   Dashboard for Sales Team
										   </div>
										   </div> */}
										  <div className="layoutSet_horizontal_component label_3">
											  Campaign 2016
										  </div>
									  </div>
								  </div>
							  </div>
							  <div id="item_type" className="item_info">
								  <div className="layoutSet_horizontal">
									  <div className="layoutSet_horizontal_group clearfix">
										  <div className="layoutSet_horizontal_label label_2">
											  Type:
										  </div>
										  <div className="layoutSet_horizontal_component label_3">
											  Bars
										  </div>
									  </div>
								  </div>
							  </div>
							  <div id="item_dataSource" className="item_info">
								  <div className="layoutSet_horizontal">
									  <div className="layoutSet_horizontal_group clearfix">
										  <div className="layoutSet_horizontal_label label_2">
											  Data Source:
										  </div>
										  <div className="layoutSet_horizontal_component label_3">
											  Hotel Data
										  </div>
									  </div>
								  </div>
							  </div>
							  <div id="item_owner" className="item_info">
								  <div className="layoutSet_horizontal">
									  <div className="layoutSet_horizontal_group clearfix">
										  <div className="layoutSet_horizontal_label label_2">
											  Owner:
										  </div>
										  <div className="layoutSet_horizontal_component label_3">
											  Qi Zheng
										  </div>
									  </div>
								  </div>
							  </div>
							  <div id="item_lastModified" className="item_info">
								  <div className="layoutSet_horizontal">
									  <div className="layoutSet_horizontal_group clearfix">
										  <div className="layoutSet_horizontal_label label_2">
											  Last Modified:
										  </div>
										  <div className="layoutSet_horizontal_component label_3">
											  Aug 22, 2016, 9:30 AM
										  </div>
									  </div>
								  </div>
							  </div>
							  <div id="item_description" className="item_info">
								  <div className="layoutSet_horizontal">
									  <div className="layoutSet_horizontal_group clearfix">
										  <div className="layoutSet_horizontal_label label_2">
											  Description:
										  </div>
										  <div className="layoutSet_horizontal_component label_3">
											  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras a tortor fermentum, condimentum nunc quis, accumsan tortor.
										  </div>
									  </div>
								  </div>
							  </div>
							  <div id="item_delete" className="item_info">
								  <div className="layoutSet_horizontal">
									  <div className="layoutSet_horizontal_group clearfix">
										  <div className="layoutSet_horizontal_label label_2">
											  &nbsp;
										  </div>
										  <div className="layoutSet_horizontal_component">
											  <div className="btnSet">
												  <div className="btn_mediun btn_secondary">
													  <div className="btn_mediun_label">Delete</div>
												  </div>
											  </div>
										  </div>
									  </div>
								  </div>
							  </div>
						  </div>
					  </div>
				  </div>
			  </div>
		  </div>
		);
	}
}

TableView.propTypes = {};
TableView.defaultProps = {};

export default TableView;
