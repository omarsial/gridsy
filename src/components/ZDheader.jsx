import React, {
	Component,
	PropTypes,
} from 'react';

import '../styles.css';

class ZDheader extends Component {
	render() {
		return (
		  <div id="topNavBar">
			  <div id="topNavBar_logo">
				  <img src="https://www.alr-research.com/styleguide_beta/img/Zoomdata_headerLogo.svg"
				       alt="Zoomdata Logo" width="98px" height="36px" />
			  </div>
			  <div id="topNavBar_btns">
				  <div id="topNavBar_btns_left">
					  <div className="topNavBar_btn">
						  
					  </div>
					  <div className="topNavBar_btn topNavBar_btn_selected">
						  
					  </div>
					  <div className="topNavBar_btn">
						  
					  </div>
				  </div>
				  <div id="topNavBar_btns_right">
					  <div className="topNavBar_btn">
						  
					  </div>
					  <div className="topNavBar_btn">
						  
					  </div>
				  </div>
			  </div>
		  </div>

		);
	}
}

ZDheader.propTypes = {};
ZDheader.defaultProps = {};

export default ZDheader;
